from enum import Enum


class KMailerK(Enum):
    MAIL_BCK_CONSOLE = 'django.core.mail.backends.console.EmailBackend'
    MAIL_BCK_DJANGO_SMTP = 'django.core.mail.backends.smtp.EmailBackend'
