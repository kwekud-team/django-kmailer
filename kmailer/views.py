from django.core import mail
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import get_object_or_404

from kommons.utils.http import get_request_site, get_referrer_url
from kmailer.forms import GenerateMailForm
from kmailer.models import MailSetting, MailTemplate, MailLog, KMailLogMixin
from kmailer.utils import KmailerUtils


class GenerateMailView(FormView):
    template_name = 'kmailer/generate_mail.html'
    form_class = GenerateMailForm
    site = None
    template_object = None
    content_object = None
    mail_template = None
    mail_setting = None
    mail_builder = None

    def dispatch(self, request, *args, **kwargs):
        self.site = get_request_site(request)
        self.content_object = self.get_content_object()
        self.template_object = self.get_template_object()
        self.mail_template = get_object_or_404(MailTemplate, pk=self.kwargs['mail_template_pk'])
        self.mail_setting = self.get_mail_setting()
        self.mail_builder = self.get_mail_builder()
        return super(GenerateMailView, self).dispatch(request, *args, **kwargs)

    def get_template_object(self):
        ct = get_object_or_404(ContentType, pk=self.kwargs['tmpt_ct_pk'])
        return ct.get_object_for_this_type(pk=self.kwargs['tmpt_pk'])

    def get_content_object(self):
        ct = get_object_or_404(ContentType, pk=self.kwargs['ctn_ct_pk'])
        return ct.get_object_for_this_type(pk=self.kwargs['ctn_pk'])

    # def get_mail_builder(self):
    #     return self.template_object.get_mail_builder(self.request, self.mail_template, self.mail_setting,
    #                                                  self.content_object)

    def get_form_kwargs(self):
        kwargs = super(GenerateMailView, self).get_form_kwargs()
        kwargs['initial'] = self.get_mail_data()
        kwargs['site'] = self.site
        return kwargs

    def form_valid(self, form):
        fields = ['subject', 'from_email', 'cc', 'bcc', 'to', 'body', 'mail_setting']
        req_data = {k: form.cleaned_data[k] for k in fields}

        res = self.send_mail(req_data)
        if res['is_valid']:
            messages.success(self.request, res['message'])
            self.success_url = get_referrer_url(self.request)
        else:
            messages.error(self.request, res['message'])
            self.success_url = self.request.path

        return super(GenerateMailView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(GenerateMailView, self).get_context_data(**kwargs)

        context.update({
            'content_object': self.content_object,
            'mail_template': self.mail_template,
            'attachments': self.mail_builder.get_attachments()
        })
        ctx = self.mail_builder.get_page_context()
        context.update(ctx)

        return context

    # def get_mail_setting(self):
    #     return MailSetting.objects.filter(
    #         site=self.site,
    #         is_active=True
    #     ).order_by('priority').first()

    def get_mail_data(self):
        email_to = self.mail_builder.get_email_to()

        return {
            'to': ','.join(email_to),
            'from_email': self.mail_builder.get_email_from(),
            'cc': self.mail_builder.get_cc_list(),
            'bcc': self.mail_builder.get_bcc_list(),
            'subject': self.mail_builder.get_subject(),
            'body': self.mail_builder.get_body(),
            'mail_setting': self.mail_setting
        }

    # def get_connection(self, mail_setting=None):
    #     mail_setting = mail_setting or self.mail_setting
    #
    #     backend = mail_setting.email_backend
    #     kw = {
    #         'host': mail_setting.email_host,
    #         'port': mail_setting.email_port,
    #         'username': mail_setting.email_user,
    #         'password': mail_setting.email_password,
    #         'use_ssl': mail_setting.email_use_ssl
    #     }
    #     return mail.get_connection(backend, **kw)

    # def send_mail(self, mail_data):
    #     mail_setting = mail_data.pop('mail_setting', None)
    #
    #     msg = EmailMultiAlternatives(**mail_data, connection=self.get_connection(mail_setting=mail_setting))
    #     attachments = self.mail_builder.get_attachments(raw_output=True)
    #     for f in attachments:
    #         msg.attach(f['filename'], f['content'], f['content_type'])
    #
    #     try:
    #         msg.send()
    #         is_valid = True
    #         resp_code = 0
    #         message = 'Email sent successfully'
    #     except Exception as e:
    #         is_valid = False
    #         resp_code = getattr(e, 'smtp_code', 999)
    #         if hasattr(e, 'smtp_error'):
    #             message = e.smtp_error.decode("utf-8")
    #         else:
    #             message = str(e)
    #
    #     extra = {
    #         'request': mail_data,
    #         'response': {'is_valid': is_valid, 'message': message, 'code': resp_code}
    #     }
    #
    #     self.save_log(extra, mail_setting=mail_setting)
    #
    #     return {
    #         'is_valid': is_valid,
    #         'message': message,
    #     }

    # def save_log(self, extra, mail_setting=None):
    #     content_type = ContentType.objects.get_for_model(self.content_object)
    #
    #     MailLog.objects.create(
    #         site=self.site,
    #         content_type=content_type,
    #         object_id=self.content_object.pk,
    #         mail_setting=mail_setting or self.mail_setting,
    #         mail_template=self.mail_template,
    #         resp_is_valid=extra['response']['is_valid'],
    #         resp_code=extra['response']['code'],
    #         resp_message=extra['response']['message'],
    #         sent_by=self.request.user,
    #         email_body=extra['request']['body'],
    #         email_from=extra['request']['from_email'],
    #         email_to=','.join(extra['request']['to']),
    #         email_cc_list=','.join(extra['request']['cc']),
    #         email_bcc_list=','.join(extra['request']['bcc'])
    #     )
    #
    #     if isinstance(self.content_object, KMailLogMixin):
    #         qs = self.content_object.get_maillog_qs()
    #         self.content_object.mail_log_count = qs.count()
    #         self.content_object.save(update_fields=['mail_log_count'])

    def get_email_body(self):
        tmpt_path = self.mail_template.template_path
        return render_to_string(f'{tmpt_path}/message.txt', {})


class DiscoverMailTemplates(TemplateView):

    def get(self, request, *args, **kwargs):
        site = get_request_site(request)
        KmailerUtils(request).discover_templates(site)

        url = get_referrer_url(request)
        return HttpResponseRedirect(url)
