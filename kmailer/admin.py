from django.contrib import admin
from django_object_actions import DjangoObjectActions

from kmailer.models import MailSetting, MailTemplate, MailLog
from kmailer.attrs import MailSettingAttr, MailTemplateAttr, MailLogAttr


@admin.register(MailSetting)
class MailSettingAdmin(admin.ModelAdmin):
    list_display = MailSettingAttr.admin_list_display
    list_filter = MailSettingAttr.admin_list_filter
    fieldsets = MailSettingAttr.admin_fieldsets
    list_editable = ['priority', 'is_active']


@admin.register(MailTemplate)
class MailTemplateAdmin(admin.ModelAdmin, DjangoObjectActions):
    list_display = MailTemplateAttr.admin_list_display
    list_filter = MailTemplateAttr.admin_list_filter
    fieldsets = MailTemplateAttr.admin_fieldsets
    actions = ['reload_templates']

    def reload_templates(self, request, queryset):
        return exp_imp.exporter.get_export_response(KuploadsK.FORMAT_EXCEL.value, queryset=queryset)
    reload_templates.short_description = 'Reload_templates'


@admin.register(MailLog)
class MailLogAdmin(admin.ModelAdmin):
    list_display = MailLogAttr.admin_list_display
    list_filter = MailLogAttr.admin_list_filter
    fieldsets = MailLogAttr.admin_fieldsets
