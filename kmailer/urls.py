from django.urls import path

from kmailer import views

app_name = 'kmailer'

urlpatterns = [
    path('templates/discover/', views.DiscoverMailTemplates.as_view(), name='discover_mail_templates'),
    path('mail/generate/<int:mail_template_pk>/tmpt-<int:tmpt_ct_pk>-<int:tmpt_pk>/ctn-<int:ctn_ct_pk>-<int:ctn_pk>/',
         views.GenerateMailView.as_view(),
         name='generate_mail'),
]