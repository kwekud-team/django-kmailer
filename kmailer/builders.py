from django.template.loader import render_to_string


class BaseMailBuilder:

    def __init__(self, request, mail_template, mail_setting, content_object):
        self.request = request
        self.mail_template = mail_template
        self.mail_setting = mail_setting
        self.content_object = content_object

    def get_state(self):
        return 'new'

    def get_page_context(self):
        return {}

    def get_email_to(self):
        return []

    def get_email_from(self):
        return self.mail_template.email_from or self.mail_setting.email_from or ''

    def get_cc_list(self):
        return self.mail_template.email_cc_list or self.mail_setting.email_cc_list or ''

    def get_bcc_list(self):
        return self.mail_template.email_bcc_list or self.mail_setting.email_bcc_list or ''

    def get_subject_template(self):
        return f'{self.mail_template.template_path}/{self.get_state()}/subject.txt'

    def get_subject_context(self):
        return {}

    def get_subject(self):
        template_name = self.get_subject_template()
        context = self.get_subject_context()
        return render_to_string(template_name, context)

    def get_body_template(self):
        return f'{self.mail_template.template_path}/{self.get_state()}/message.txt'

    def get_body_context(self):
        return {}

    def get_body(self):
        template_name = self.get_body_template()
        context = self.get_body_context()
        return render_to_string(template_name, context)

    def get_attachments(self, **kwargs):
        return []
