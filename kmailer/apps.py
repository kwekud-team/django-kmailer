from django.apps import AppConfig


class KmailerConfig(AppConfig):
    name = 'kmailer'
