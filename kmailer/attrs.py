
class MailSettingAttr:
    admin_list_display = ['site', 'email_backend', 'priority', 'email_host', 'email_user', 'email_port', 'is_active']
    admin_list_filter = ['email_backend', 'priority', 'is_active']
    admin_fieldsets = [
        ('Basic', {'fields': ('site', 'email_backend', 'email_host', 'email_user', 'email_password', 'email_port',
                              'email_use_ssl')}),
        ('Extra', {'fields': ['email_from', 'email_cc_list', 'email_bcc_list', 'priority',]}),
    ]


class MailTemplateAttr:
    admin_list_display = ['site', 'code', 'state', 'email_subject', 'template_path', 'email_from', 'email_cc_list',
                          'email_bcc_list']
    admin_list_filter = ['date_created']
    admin_fieldsets = [
        ('Basic', {'fields': ('site', 'code', 'state', 'template_path', 'email_body', 'email_subject')}),
        ('Extra', {'fields': ['email_from', 'email_cc_list', 'email_bcc_list']}),
    ]


class MailLogAttr:
    admin_list_display = ['site', 'mail_setting', 'mail_template', 'resp_is_valid', 'resp_code',
                          'sent_by', 'email_from', 'email_to', 'content_object']
    admin_list_filter = ['date_created']
    admin_fieldsets = [
        ('Basic', {'fields': ('mail_setting', 'mail_template', 'email_body', 'email_from', 'email_to', 'email_cc_list',
                              'email_bcc_list')}),
        ('Extra', {'fields': ['resp_is_valid', 'resp_code', 'resp_message', 'sent_by', 'parent']}),
    ]
