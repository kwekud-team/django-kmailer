from django.db import models
from django.urls import reverse
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from kmailer.constants import KMailerK
from kmailer.builders import BaseMailBuilder


BACKENDS = (
    (KMailerK.MAIL_BCK_CONSOLE.value, 'Console'),
    (KMailerK.MAIL_BCK_DJANGO_SMTP.value, 'Django SMTP'),
)


class MailSetting(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    sort = models.PositiveIntegerField(default=1000)

    email_backend = models.CharField(max_length=250, choices=BACKENDS)
    email_from = models.CharField(max_length=100)
    email_cc_list = models.TextField(null=True, blank=True)
    email_bcc_list = models.TextField(null=True, blank=True)

    email_host = models.CharField(max_length=250, null=True, blank=True)
    email_user = models.CharField(max_length=250, null=True, blank=True)
    email_password = models.CharField(max_length=250, null=True, blank=True)
    email_port = models.PositiveSmallIntegerField(default=0)
    email_use_ssl = models.BooleanField(default=False)
    priority = models.PositiveSmallIntegerField(default=1000)

    def __str__(self):
        vals = [self.email_backend, self.email_host, self.email_user]
        return ', '.join([str(x) for x in vals if x])

    class Meta:
        ordering = ('priority',)
        unique_together = ('site', 'email_backend', 'email_host', 'priority',)


class MailTemplate(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    sort = models.PositiveIntegerField(default=1000)
    code = models.CharField(max_length=250)
    state = models.CharField(max_length=50)
    email_subject = models.CharField(max_length=250, null=True, blank=True)
    email_body = models.TextField()
    template_path = models.CharField(max_length=250)

    email_from = models.CharField(max_length=100, null=True, blank=True)
    email_cc_list = models.TextField(null=True, blank=True)
    email_bcc_list = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s: %s' % (self.code, self.date_created)

    class Meta:
        unique_together = ('site', 'code', 'state')

    def generate_mail_url(self, template_object, content_object):
        template_ct = ContentType.objects.get_for_model(template_object)
        content_ct = ContentType.objects.get_for_model(content_object)

        return reverse('kmailer:generate_mail', args=(self.pk, template_ct.pk, template_object.pk,
                                                      content_ct.pk, content_object.pk))


class MailLog(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    sort = models.PositiveIntegerField(default=1000)

    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey()

    mail_setting = models.ForeignKey(MailSetting, on_delete=models.CASCADE)
    mail_template = models.ForeignKey(MailTemplate, on_delete=models.CASCADE)
    resp_is_valid = models.BooleanField(default=False)
    resp_code = models.CharField(max_length=20, null=True, blank=True)
    resp_message = models.TextField()
    sent_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    email_body = models.TextField()
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    email_from = models.CharField(max_length=100)
    email_to = models.TextField()
    email_cc_list = models.TextField(null=True, blank=True)
    email_bcc_list = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s: %s' % (self.pk, self.date_created)


class KMailTemplateMixin(models.Model):
    mail_template_code = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        abstract = True

    def get_mail_template(self, site, state):
        return MailTemplate.objects.filter(
            site=site,
            code__iexact=self.mail_template_code,
            state__iexact=state
        ).first()

    def get_mail_builder(self, request, mail_template, mail_setting, content_object):
        return BaseMailBuilder(request, mail_template, mail_setting, content_object)


class KMailLogMixin(models.Model):
    mail_log_count = models.PositiveSmallIntegerField(default=0)

    class Meta:
        abstract = True

    def get_maillog_qs(self):
        content_type = ContentType.objects.get_for_model(self)
        return MailLog.objects.filter(object_id=self.pk, content_type=content_type)
