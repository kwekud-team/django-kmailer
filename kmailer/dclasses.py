from dataclasses import dataclass, field
from typing import List


@dataclass
class Response:
    is_valid: bool
    resp_code: str
    resp_msg: str

    def to_dict(self):
        return {
            'is_valid': self.is_valid,
            'resp_code': self.resp_code,
            'resp_msg': self.resp_msg
        }


@dataclass
class MailData:
    sender: str
    subject: str
    message: str
    recipients: List[str]
    cc: List[str] = field(default_factory=list)
    bcc: List[str] = field(default_factory=list)

    def to_dict(self):
        return {
            'to': self.recipients,
            'from_email': self.sender,
            'cc': self.cc,
            'bcc': self.bcc,
            'subject': self.subject,
            'body': self.message,
        }


@dataclass
class TemplateData:
    code: str
    state: str
    template_path: str = ''
