from django import forms
from kmailer.models import MailSetting


class GenerateMailForm(forms.Form):
    subject = forms.CharField()
    from_email = forms.EmailField()
    to = forms.CharField()
    cc = forms.CharField(required=False)
    bcc = forms.CharField(required=False)
    body = forms.CharField(widget=forms.Textarea)
    mail_setting = forms.ModelChoiceField(queryset=MailSetting.objects.none())
    # attachments = forge.CharField(required=False, widget=ReadOnlyWidget)

    from_email.widget.attrs.update({'class': 'form-control form-control-sm straight-edges'})
    subject.widget.attrs.update({'class': 'form-control form-control-sm straight-edges'})
    to.widget.attrs.update({'class': 'form-control form-control-sm straight-edges'})
    cc.widget.attrs.update({'class': 'form-control form-control-sm straight-edges'})
    bcc.widget.attrs.update({'class': 'form-control form-control-sm straight-edges'})
    body.widget.attrs.update({'class': 'form-control form-control-sm straight-edges', 'rows': '15'})

    def __init__(self, *args, **kwargs):
        site = kwargs.pop('site', None)
        super(GenerateMailForm, self).__init__(*args, **kwargs)

        if site:
            qs = MailSetting.objects.filter(site=site, is_active=True)
            self.fields['mail_setting'].queryset = qs

    def clean_to(self):
        return self.cleaned_data['to'].split(',')

    def clean_cc(self):
        return self.cleaned_data['cc'].split(',')

    def clean_bcc(self):
        return self.cleaned_data['bcc'].split(',')
