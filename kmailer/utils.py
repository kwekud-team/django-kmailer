import os
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.apps import apps
from django.core.mail import send_mail
from django.contrib.contenttypes.models import ContentType

from kommons.utils.http import get_request_site
from kommons.utils.files import get_sub_directories
from kmailer.models import MailTemplate, MailSetting, MailLog, KMailLogMixin
from kmailer.dclasses import Response, MailData, TemplateData
from kmailer.builders import BaseMailBuilder


class SendMailUtils:

    def __init__(self, request):
        self.request = request
        self.site = get_request_site(request)

    def send_mail(self, content_object, mail_data: MailData, template_data: TemplateData, user):
        mail_setting = self.get_mail_setting()
        mail_template = self.get_mail_template(template_data)
        mail_builder = self.get_mail_builder(content_object, mail_template, mail_setting)

        msg = EmailMultiAlternatives(**mail_data.to_dict(), connection=self.get_connection(mail_setting))
        attachments = mail_builder.get_attachments(raw_output=True)
        for f in attachments:
            msg.attach(f['filename'], f['content'], f['content_type'])

        try:
            msg.send()
            is_valid = True
            resp_code = 0
            message = 'Email sent successfully'
        except Exception as e:
            is_valid = False
            resp_code = getattr(e, 'smtp_code', 999)
            if hasattr(e, 'smtp_error'):
                message = e.smtp_error.decode("utf-8")
            else:
                message = str(e)

        response = Response(is_valid=is_valid, resp_msg=message, resp_code=resp_code)
        extra = {
            'request': mail_data.to_dict(),
            'response': response.to_dict()
        }

        self.save_log(extra, content_object, mail_template, mail_setting, user)
        return response

    def get_connection(self, mail_setting):
        backend = mail_setting.email_backend
        kw = {
            'host': mail_setting.email_host,
            'port': mail_setting.email_port,
            'username': mail_setting.email_user,
            'password': mail_setting.email_password,
            'use_ssl': mail_setting.email_use_ssl
        }
        return mail.get_connection(backend, **kw)

    def get_mail_template(self, template_data: TemplateData):
        return MailTemplate.objects.filter(
            site=self.site,
            code__iexact=template_data.code,
            state__iexact=template_data.state
        ).first()

    def get_mail_builder(self, content_object, mail_template, mail_setting):
        return BaseMailBuilder(self.request, mail_template, mail_setting, content_object)

    def get_mail_setting(self):
        return MailSetting.objects.filter(
            site=self.site,
            is_active=True
        ).order_by('priority').first()

    def save_log(self, extra, content_object, mail_template, mail_setting, user):
        content_type = ContentType.objects.get_for_model(content_object)

        MailLog.objects.create(
            site=self.site,
            content_type=content_type,
            object_id=content_object.pk,
            mail_setting=mail_setting,
            mail_template=mail_template,
            resp_is_valid=extra['response']['is_valid'],
            resp_code=extra['response']['resp_code'],
            resp_message=extra['response']['resp_msg'],
            sent_by=user,
            email_body=extra['request']['body'],
            email_from=extra['request']['from_email'],
            email_to=','.join(extra['request']['to']),
            email_cc_list=','.join(extra['request']['cc']),
            email_bcc_list=','.join(extra['request']['bcc'])
        )

        if isinstance(content_object, KMailLogMixin):
            qs = content_object.get_maillog_qs()
            content_object.mail_log_count = qs.count()
            content_object.save(update_fields=['mail_log_count'])


class KmailerUtils:

    def __init__(self, request):
        self.request = request

    def send_mail(self, template_prefix, from_email, recipient_list, template_context, fail_silently=False, **kwargs):
        preview = self.request.GET.get('preview')

        if preview:
            return render(self.request, f'{template_prefix}.html', template_context)

        send_mail(template_prefix, from_email, recipient_list, template_context, fail_silently=fail_silently, **kwargs)

    def discover_templates(self, site):
        for obj in apps.get_app_configs():
            app_name = obj.name.split('.')[-1]
            template_path = os.path.join(obj.path, f'templates/{app_name}/kmailer')
            if os.path.exists(template_path):

                for template_dir in get_sub_directories(template_path):
                    code = template_dir
                    code_path = os.path.join(template_path, template_dir)
                    for template_state in get_sub_directories(code_path):

                        file_path = os.path.join(template_path, template_dir, template_state)
                        email_subject = open(os.path.join(file_path, 'subject.txt'), 'r').read()
                        email_body = open(os.path.join(file_path, 'message.txt'), 'r').read()
                        tmpt_path = f'{app_name}/kmailer/{template_dir}'

                        MailTemplate.objects.get_or_create(
                            site=site,
                            code=code,
                            state=template_state,
                            defaults={
                                'template_path': tmpt_path,
                                'email_subject': email_subject,
                                'email_body': email_body
                            })
